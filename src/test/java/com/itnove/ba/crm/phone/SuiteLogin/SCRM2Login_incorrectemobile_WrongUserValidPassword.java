package com.itnove.ba.crm.phone.SuiteLogin;

import com.itnove.ba.BaseTest;
import com.itnove.ba.BaseTestSauce;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class SCRM2Login_incorrectemobile_WrongUserValidPassword extends BaseTestSauce {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user1", "bitnami");

        WebElement liniaError = driver.findElement(By.xpath("/html/body"));
        wait.until(ExpectedConditions.visibilityOf(liniaError));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/CRM/SCRM2_FF.png"));



    }
}
