package com.itnove.ba.crm.phone.SuiteLogin;

import com.itnove.ba.BaseTestSauce;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.testng.Assert.assertTrue;

public class SCRM1Login_CorrecteTestmobile extends BaseTestSauce {

    @Test
    public void testAppComp() throws InterruptedException {
        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        WebElement titleHome = driver.findElement(By.id("moduleTab_Home"));

        assertTrue(titleHome.isDisplayed());
        //assertTrue((driver.getCurrentUrl().toLowerCase().contains("home")));


    }
}
