package com.itnove.ba.crm.computer.SuiteDashboard;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageDashboard;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.testng.annotations.Test;

import java.io.IOException;

public class SCRM14Dash_Lupa extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        Thread.sleep(3000);

        PageDashboard pageDashboard = new PageDashboard(driver);
        pageDashboard.lupa("Ripoll", hover);
        pageDashboard.lupa("Cristina", hover);

    }

}



