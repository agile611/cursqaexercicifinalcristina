package com.itnove.ba.crm.computer.SuiteCreate.CreateDocument;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageCreateDocument;
import com.itnove.ba.crm.pages.PageDashboard;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;


public class SCRM27CreateDocument_MandatoryFields_Correcte extends BaseTest{

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        PageDashboard pageDashboard = new PageDashboard(driver);
        pageDashboard.createButtonClick(hover);
        pageDashboard.hoverAndClick(pageDashboard.createDocument, hover);

        PageCreateDocument pageCreateDocument = new PageCreateDocument(driver);
        File file = new File(File.separator + "Users" + File.separator +
                "nasrodo" + File.separator + "IdeaProjects" + File.separator +
                "cursqaexercicifinalcristina" + File.separator + "src" + File.separator +
                "main" + File.separator + "resources" + File.separator + "document B.docx");        pageCreateDocument.browseFileName(file.getAbsolutePath());

        pageCreateDocument.fillInFieldsMandatoryFieldsCorrect();

        pageCreateDocument.saveDocument(hover);

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/SCRM30_FF.png"));

    }

}
