package com.itnove.ba.crm.computer.SuiteCreate.CreateDocument;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageCreateDocument;
import com.itnove.ba.crm.pages.PageDashboard;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;


public class SCRM26CreateDocument_MandatoryFields_Incorrecte extends BaseTest{

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        PageDashboard pageDashboard = new PageDashboard(driver);
        pageDashboard.createButtonClick(hover);
        pageDashboard.hoverAndClick(pageDashboard.createDocument, hover);

        PageCreateDocument pageCreateDocument = new PageCreateDocument(driver);
        pageCreateDocument.fillInFieldsMandatoryFieldsIncorrect();

        pageCreateDocument.saveDocument(hover);

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/SCRM31_FF.png"));

    }

}
