package com.itnove.ba.crm.computer.SuiteLogin;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.io.File;
import java.io.IOException;

public class SCRM4Login_incorrecte_ValidUserWrongPassword extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami1");

        WebElement liniaError = driver.findElement(By.xpath("/html/body"));
        wait.until(ExpectedConditions.visibilityOf(liniaError));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/CRM/SCRM4_FF.png"));

    }
}
