package com.itnove.ba.crm.computer.SuiteCreate.CreateContact;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageCreateContact;
import com.itnove.ba.crm.pages.PageDashboard;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.testng.annotations.Test;

import java.io.IOException;


public class SCRM21CreateContact_RecoverData extends BaseTest{

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        PageDashboard pageDashboard = new PageDashboard(driver);
        pageDashboard.createButtonClick(hover);
        pageDashboard.hoverAndClick(pageDashboard.createContact, hover);

        PageCreateContact pageCreateContact = new PageCreateContact(driver);
        pageCreateContact.recoverDataContactIntroContact(hover);

        pageDashboard.lupa("marti", hover);

    }

}
