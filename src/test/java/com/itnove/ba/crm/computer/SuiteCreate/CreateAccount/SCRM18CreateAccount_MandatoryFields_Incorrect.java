package com.itnove.ba.crm.computer.SuiteCreate.CreateAccount;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageCreateAccount;
import com.itnove.ba.crm.pages.PageDashboard;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.apache.commons.io.FileUtils;
import org.testng.annotations.Test;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.io.IOException;


public class SCRM18CreateAccount_MandatoryFields_Incorrect extends BaseTest{

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        PageDashboard pageDashboard = new PageDashboard(driver);
        pageDashboard.createButtonClick(hover);
        pageDashboard.hoverAndClick(pageDashboard.createAccount, hover);

        PageCreateAccount pageCreateAccount = new PageCreateAccount(driver);
        pageCreateAccount.fillInFieldsMandatoryFieldsIncorrect(hover);

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/CRM/SCRM18_FF.png"));

    }

}
