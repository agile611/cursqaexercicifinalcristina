package com.itnove.ba.crm.computer.SuiteCreate.CreateAccount;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageCreateAccount;
import com.itnove.ba.crm.pages.PageDashboard;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.testng.annotations.Test;

import java.io.IOException;


public class SCRM16CreateAccount_IntroData extends BaseTest{

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        PageDashboard pageDashboard = new PageDashboard(driver);
        pageDashboard.createButtonClick(hover);
        pageDashboard.hoverAndClick(pageDashboard.createAccount, hover);

        PageCreateAccount pageCreateAccount = new PageCreateAccount(driver);
        pageCreateAccount.fillInFields(hover);

    }

}
