package com.itnove.ba.crm.computer.SuiteDashboard;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageDashboard;
import com.itnove.ba.crm.pages.PageLoginCRM;
import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import static org.testng.Assert.assertTrue;

import java.io.IOException;



public class SCRM13Dash_Logout extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");

        Thread.sleep(3000);

        PageDashboard pageDashboard = new PageDashboard(driver);
        pageDashboard.funcioLogOut(hover);

        Thread.sleep(6000);

        assertTrue(pageLoginCRM.quadreTextPassword.isDisplayed());

    }

}





