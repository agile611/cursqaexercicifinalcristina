package com.itnove.ba.crm.computer.SuiteLogin;

import com.itnove.ba.BaseTest;
import com.itnove.ba.crm.pages.PageLoginCRM;
import static org.testng.Assert.assertTrue;
import org.testng.annotations.Test;

public class SCRM1Login_CorrecteTest extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageLoginCRM pageLoginCRM = new PageLoginCRM(driver);
        pageLoginCRM.login("user", "bitnami");
        assertTrue(driver.getCurrentUrl().toLowerCase().contains("home"));

    }
}
