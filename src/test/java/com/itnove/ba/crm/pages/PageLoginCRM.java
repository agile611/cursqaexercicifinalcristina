package com.itnove.ba.crm.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PageLoginCRM {

    private WebDriver driver;
    public WebDriverWait wait;

    @FindBy(id = "user_name")
    public WebElement quadreTextUser;

    @FindBy(id = "user_password")
    public WebElement quadreTextPassword;

    @FindBy(id = "bigbutton")
    public WebElement botoLogin;

    public void login(String user, String pswd) throws InterruptedException {
        driver.navigate().to("http://crm.votarem.lu");
        quadreTextUser.click();
        quadreTextUser.sendKeys(user);
        quadreTextPassword.click();
        quadreTextPassword.sendKeys(pswd);
        botoLogin.click();
        Thread.sleep(3000);
    }

    public PageLoginCRM(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;}

}
