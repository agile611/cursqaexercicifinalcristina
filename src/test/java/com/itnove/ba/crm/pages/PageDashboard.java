package com.itnove.ba.crm.pages;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;


public class PageDashboard {

    private WebDriver driver;
    public WebDriverWait wait;

    // BARRA NAVEGACIÓ SUPERIOR ESQUERRA

    @FindBy(xpath = "/html/body/div[2]/nav/div/div[2]/ul/li[1]")
    public WebElement homeIcon;

    @FindBy(id = "grouptab_0")
    public WebElement sales;

    @FindBy(id = "grouptab_1")
    public WebElement marketing;

    @FindBy(id = "grouptab_4")
    public WebElement collaboration;

    // BARRA NAVEGACIÓ SUPERIOR DRETA

    //ALL
    @FindBy(xpath = "id(\"ajaxHeader\")/nav/div/div/button")
    public WebElement allDesplegable;

    //CREATE

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/a)[2]")
    public WebElement createButton;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul)[2]/li[1]/a")
    public WebElement createAccount;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul)[2]/li[2]/a")
    public WebElement createContact;

    @FindBy(xpath = "(.//*[@id='quickcreatetop']/ul)[2]/li[5]/a")
    public WebElement createDocument;

    //LUPA

    @FindBy(xpath = "(.//*[@id='searchbutton'])[2]")
    public WebElement lupa;

    @FindBy(xpath = "(.//*[@id='query_string'])[3]")
    public WebElement quadreTextLupa;

    @FindBy(xpath = "(.//*[@id='searchformdropdown']/div/span/button)[2]")
    public WebElement lupaPetita;

    @FindBy(xpath = "id('pagecontent')/form/input[3]")
    public WebElement quadreCercaLupaSecundari;

    @FindBy(xpath = "id('pagecontent')/table[1]/tbody[2]/tr/td[2]/a")
    public WebElement nomPrimerResultat;

    @FindBy(xpath = "id('pagecontent')/div[1]/h2[1]")
    public WebElement titolResultat;

    //NOTIFICATIONS

    //USUARI

    //@FindBy(xpath = "id('ajaxHeader')/nav[1]/div[1]/div[5]/ul/li[5]")
    //public WebElement userIcon;

    //@FindBy(id = "usermenucollapsed")
    //public WebElement userIcon;

    @FindBy(xpath = "(.//*[@id='usermenucollapsed'])[2]")
    public WebElement userIcon;

    //@FindBy(xpath = "id('ajaxHeader')/nav[1]/div[1]/div[5]/ul/li[5]/ul/li[6]")
    //public WebElement logOut;

    @FindBy(xpath = "(.//*[@id='logout_link'])[2]")
    public WebElement logOut;

    //@FindBy(xpath = "id(\"globalLinks\")/ul/li[6]/a")
    //public WebElement logOut;

    // DINS CADA PLANA
    @FindBy(xpath = "id(\"modulelinks\")/span/a")
    public WebElement title;



    //*****************************


    //BARRA NAVEGACIÓ SUPERIOR ESQUERRA

    public void HoverEsquerraTopNav(Actions hover) {
        List<WebElement> topNavEsquerra = driver.findElements(By.className("topnav"));
        System.out.println(topNavEsquerra.size());

        for (int i = 2; i < (topNavEsquerra.size() + 2); i++) {
            WebElement elementTopNavEsquerra = driver.
                    findElement(By.xpath(".//*[@id=\"toolbar\"]/ul[1]/li[" + i + "]"));

            hover.moveToElement(elementTopNavEsquerra).build().perform();

        }
    }

    public void clickSalesDesplegableFullScreen() throws InterruptedException {

        List<WebElement> desplegarSales = driver.findElements(By.xpath("id('grouptab_0')/../ul/li"));
        Actions hover = new Actions(driver);

        for (int i = 1; i < (desplegarSales.size()+1); i++) {
            String current = driver.getCurrentUrl();
            hover.moveToElement(sales).click().build().perform();
            Thread.sleep(2000);
            WebElement elementSales = driver.
                    findElement(By.xpath("id('grouptab_0')/../ul/li[" + i + "]"));
            hover.moveToElement(sales).moveToElement(elementSales).click().build().perform();
            Thread.sleep(2000);
            assertTrue(!(current.equals(driver.getCurrentUrl())));
        }
    }
    public void clickAllDesplegable() throws InterruptedException {

        List<WebElement> desplegarAll = driver.findElements(By.xpath("id('mobile_menu')/li"));
        Actions hover = new Actions(driver);
        System.out.println(desplegarAll.size());

        for (int i = 2; i < (desplegarAll.size()+1); i++) {
            System.out.println(i);
            String current = title.getAttribute("module");
            hover.moveToElement(allDesplegable).click().build().perform();
            Thread.sleep(2000);
            WebElement elementAll = driver.
                    findElement(By.xpath("id(\"mobile_menu\")/li[" + i + "]"));
            hover.moveToElement(elementAll).moveToElement(elementAll).click().build().perform();
            Thread.sleep(2000);
            assertFalse(title.getAttribute("module").equals(current));
        }
    }

    public void chooseFromSales(String chosenElement) throws InterruptedException {

        Actions hover = new Actions(driver);

        hover.moveToElement(sales).click().build().perform();
        Thread.sleep(6000);

        List<WebElement> desplegarSales = driver.findElements(By.xpath("id('grouptab_0')/../ul/li"));

        for (int i = 1; i < (desplegarSales.size() + 1); i++) {

            WebElement elementSales = driver.
                    findElement(By.xpath("id('grouptab_0')/../ul/li[" + i + "]/a"));

            if (elementSales.getAttribute("module").contains(chosenElement)) {
                hover.moveToElement(elementSales).click().build().perform();

                break;
            }
        }
    }

    public void clickMarketingDesplegable() throws InterruptedException {

        List<WebElement> desplegarMarketing = driver.findElements(By.xpath("id('grouptab_1')/../ul/li"));
        Actions hover = new Actions(driver);

        for (int i = 1; i < (desplegarMarketing.size()+1); i++) {
            String current = driver.getCurrentUrl();
            hover.moveToElement(marketing).click().build().perform();
            Thread.sleep(2000);
            WebElement elementMarketing = driver.
                    findElement(By.xpath("id('grouptab_1')/../ul/li[" + i + "]"));
            hover.moveToElement(marketing).moveToElement(elementMarketing).click().build().perform();
            Thread.sleep(2000);
            assertTrue(!(current.equals(driver.getCurrentUrl())));
        }

    }

    public void clickCollaborationDesplegable() throws InterruptedException {

        List<WebElement> desplegarCollaboration = driver.findElements(By.xpath("id('grouptab_4')/../ul/li"));
        Actions hover = new Actions(driver);

        for (int i = 1; i < (desplegarCollaboration.size()+1); i++) {
            String current = driver.getCurrentUrl();
            hover.moveToElement(collaboration).click().build().perform();
            Thread.sleep(2000);
            WebElement elementCollaboration = driver.
                    findElement(By.xpath("id('grouptab_4')/../ul/li[" + i + "]"));
            hover.moveToElement(marketing).moveToElement(elementCollaboration).click().build().perform();
            Thread.sleep(2000);
            assertTrue(!(current.equals(driver.getCurrentUrl())));
        }

    }

    //LUPA

   public void lupa(String textCerca, Actions hover) throws InterruptedException {

       wait = (new WebDriverWait(driver, 10));

       hover.moveToElement(lupa).click().build().perform();
       quadreTextLupa.click();
       quadreTextLupa.sendKeys(textCerca);
       lupaPetita.click();

       assertTrue(quadreCercaLupaSecundari.getAttribute("value").equals(textCerca));

       nomPrimerResultat.click();
       wait.until(ExpectedConditions.visibilityOf(titolResultat));
       assertTrue(titolResultat.getText().contains(textCerca.toUpperCase()));

   }

   //CREATE

    public void createButtonClick(Actions hover) throws InterruptedException {
           hover.moveToElement(createButton)
                   .moveToElement(createButton)
                   .click().build().perform();
    }

    public void hoverAndClickEveryCreate(WebDriver driver, Actions hover) throws InterruptedException {

        //createButtonClick(hover);
        List<WebElement> listElements = driver.findElements(By.xpath("id(\"quickcreatetop\")/ul/li"));
        //String listElements = "id(\"quickcreatetop\")/ul/li/a";
        //String lsl = listElements + "/a";
        //List<WebElement> listOfCreates = driver.findElements(By.xpath(lsl));
        for (int i = 1; i <= listElements.size(); i++) {
            createButtonClick(hover);
            Thread.sleep(1000);
            WebElement eachCreateItem = driver.findElement(By.xpath("id('quickcreatetop')/ul[1]/li[" + i + "]/a[1]"));
            hover.moveToElement(eachCreateItem)
                    .moveToElement(eachCreateItem)
                    .click().build().perform();
            eachCreateItem.click();
            Thread.sleep(3000);
            System.out.println(i);
        }
    }

    public void hoverAndClick(WebElement element, Actions hover) {

        hover.moveToElement(element)
                .moveToElement(element)
                .click().build().perform();
    }

    //ICONA USUARI

    public void funcioLogOut(Actions hover) throws InterruptedException {

        hover.moveToElement(userIcon).moveToElement(userIcon).click().build().perform();
        Thread.sleep(4000);
        hover.moveToElement(logOut).moveToElement(logOut).click().build().perform();

   }

    public PageDashboard(WebDriver driver) {

        PageFactory.initElements(driver, this);
        this.driver = driver;
    }
}