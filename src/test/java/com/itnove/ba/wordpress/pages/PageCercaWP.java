package com.itnove.ba.wordpress.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PageCercaWP {

    private WebDriver driver;
    public WebDriverWait wait;

    @FindBy(xpath = "id(\"masthead\")/div/div[2]/div/a")
    public WebElement scrollDown;

    @FindBy(xpath = "id(\"search-2\")/form/label")
    public WebElement quadreTextCercaParent;

    @FindBy(xpath = "id(\"search-2\")/form[1]/button[1]")
    public WebElement lupa;

    @FindBy(xpath = "id('main')/article/div")
    public WebElement articleObertPagina;

    @FindBy(xpath = "id('main')/article/div[2]")
    public WebElement articleObertPost;

    @FindBy(xpath = "id(\"main\")/p")
    public WebElement alertaCercaUnsuccessful;



    public void cerca(String textCerca, Actions hover) throws InterruptedException {

        driver.navigate().to("http://wordpress.votarem.lu");

        hover.moveToElement(scrollDown).click().build().perform();

        Thread.sleep(2000);

        String quadreTextCercaString = quadreTextCercaParent.getAttribute("for");
        WebElement quadreTextCerca = driver.findElement(By.id(quadreTextCercaString));

        hover.moveToElement(quadreTextCerca).click().build().perform();
        quadreTextCerca.sendKeys(textCerca);

        hover.moveToElement(lupa).click().build().perform();

    }

    public boolean comprovar(String textCerca, Actions hover) {

        if (alertaCercaUnsuccessful.isDisplayed()) {

            return false;

        } else {

            List<WebElement> resultatsCerca = driver.findElements(By.xpath("id(\"main\")/article"));

            for (int i = 1; i < (resultatsCerca.size() + 1); i++) {

                WebElement resultatCercaDeTornTítol =
                        driver.findElement(By.xpath("id('main')/article[" + i + "]/header/h2"));

                WebElement resultatCercaDeTornResum =
                        driver.findElement(By.xpath("id('main')/article[" + i + "]/div"));

                if (resultatCercaDeTornTítol.getText().toLowerCase().contains(textCerca.toLowerCase())) {

                    return true;

                } else if (resultatCercaDeTornResum.getText().toLowerCase().contains(textCerca.toLowerCase())) {

                    return true;

                } else {

                    try {

                        WebElement continuaLlegint =
                                driver.findElement(By.xpath("id('main')/article[" + i + "]/div/p[2]/a"));

                        hover.moveToElement(continuaLlegint).click().build().perform();

                        // wait.until(ExpectedConditions.visibilityOf(articleObertPagina));

                        Thread.sleep(3000);

                        if (articleObertPagina.isDisplayed() &&
                                (articleObertPagina.getText().toLowerCase().contains(textCerca.toLowerCase()))) {

                            return true;

                        } else if (articleObertPost.getText().toLowerCase().contains(textCerca.toLowerCase())) {

                            return true;

                        }

                    } catch (Exception e) {

                        System.out.println("Something didn't work");
                        return false;

                    }

                }

                return true;
            }

            return true;

        }

    }

    public PageCercaWP(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;}
}

