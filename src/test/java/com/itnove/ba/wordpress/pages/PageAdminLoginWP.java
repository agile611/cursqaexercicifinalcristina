package com.itnove.ba.wordpress.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageAdminLoginWP {

    private WebDriver driver;
    public WebDriverWait wait;

    @FindBy(id = "user_login")
    public WebElement quadreTextUser;

    @FindBy(id = "user_pass")
    public WebElement quadreTextPassword;

    @FindBy(id = "wp-submit")
    public WebElement loginButton;


    public void login(String user, String pswd, Actions hover) throws InterruptedException {

        driver.navigate().to("http://wordpress.votarem.lu/admin");

        hover.moveToElement(quadreTextUser).click().build().perform();
        quadreTextUser.sendKeys(user);

        hover.moveToElement(quadreTextPassword).click().build().perform();
        quadreTextPassword.sendKeys(pswd);

        hover.moveToElement(loginButton).click().build().perform();

        Thread.sleep(2000);

    }

    public PageAdminLoginWP(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;}

}

