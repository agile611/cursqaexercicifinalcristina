package com.itnove.ba.wordpress.computer.SuiteLogin;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import com.itnove.ba.wordpress.pages.PageAdminLoginWP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WP1Login_CorrecteTest extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageAdminLoginWP pageAdminLoginWP = new PageAdminLoginWP(driver);
        pageAdminLoginWP.login("admin", "admin", hover);

        assertTrue((driver.getCurrentUrl().contains("admin")));
    }
}
