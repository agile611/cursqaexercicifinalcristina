package com.itnove.ba.wordpress.computer.SuiteCerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageCercaWP;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WP10Cerca_Incorrecte_URL extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageCercaWP pageCercaWP = new PageCercaWP(driver);
        pageCercaWP.cerca("http://wordpress.votarem.lu/?s=WIKIPEDIA", hover);

        boolean textCercaPresent = pageCercaWP.comprovar("http://wordpress.votarem.lu/?s=WIKIPEDIA", hover);

        Assert.assertFalse(textCercaPresent == true);

    }
}
