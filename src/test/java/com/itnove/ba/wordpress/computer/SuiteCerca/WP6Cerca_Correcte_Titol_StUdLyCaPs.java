package com.itnove.ba.wordpress.computer.SuiteCerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageCercaWP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WP6Cerca_Correcte_Titol_StUdLyCaPs extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageCercaWP pageCercaWP = new PageCercaWP(driver);
        pageCercaWP.cerca("WiKiPeDiA", hover);

        boolean textCercaPresent = pageCercaWP.comprovar("WiKiPeDiA", hover);

        assertTrue(textCercaPresent == true);

    }
}
