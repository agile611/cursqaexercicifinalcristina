package com.itnove.ba.wordpress.computer.SuiteCerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageAdminLoginWP;
import com.itnove.ba.wordpress.pages.PageCercaWP;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertTrue;

public class WP5Cerca_Correcte_Titol extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageCercaWP pageCercaWP = new PageCercaWP(driver);
        pageCercaWP.cerca("Wikipedia", hover);

        boolean textCercaPresent = pageCercaWP.comprovar("Wikipedia", hover);

        assertTrue(textCercaPresent == true);

    }
}
