package com.itnove.ba.wordpress.computer.SuiteCerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageCercaWP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WP8Cerca_Correcte_ArticleBody extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageCercaWP pageCercaWP = new PageCercaWP(driver);
        pageCercaWP.cerca("CMMI", hover);

        boolean textCercaPresent = pageCercaWP.comprovar("CMMI", hover);

        assertTrue(textCercaPresent == true);

    }
}
