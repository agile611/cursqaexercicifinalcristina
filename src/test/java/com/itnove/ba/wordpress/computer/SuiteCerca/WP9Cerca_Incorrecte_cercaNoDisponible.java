package com.itnove.ba.wordpress.computer.SuiteCerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageCercaWP;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WP9Cerca_Incorrecte_cercaNoDisponible extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageCercaWP pageCercaWP = new PageCercaWP(driver);
        pageCercaWP.cerca("unicorni", hover);

        boolean textCercaPresent = pageCercaWP.comprovar("unicorni", hover);

        Assert.assertFalse(textCercaPresent == true);

    }
}
