package com.itnove.ba.wordpress.computer.SuiteLogin;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageAdminLoginWP;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertTrue;

public class WP2Login_Incorrecte_ValidUserWrongPassword extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageAdminLoginWP pageAdminLoginWP = new PageAdminLoginWP(driver);
        pageAdminLoginWP.login("admin", "admin1", hover);

        WebElement liniaError = driver.findElement(By.id("login_error"));
        wait.until(ExpectedConditions.visibilityOf(liniaError));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/WP/WP2_FF.png"));
    }
}
