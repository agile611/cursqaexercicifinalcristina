package com.itnove.ba.wordpress.computer.SuiteCerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageCercaWP;
import org.testng.Assert;
import org.testng.annotations.Test;

public class WP11Cerca_Incorrecte_numeros extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageCercaWP pageCercaWP = new PageCercaWP(driver);
        pageCercaWP.cerca("736229193756235325852592", hover);

        boolean textCercaPresent = pageCercaWP.comprovar("736229193756235325852592", hover);

        Assert.assertFalse(textCercaPresent == true);

    }
}
