package com.itnove.ba.wordpress.computer.SuiteCerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.wordpress.pages.PageCercaWP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class WP7Cerca_Correcte_Resum extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageCercaWP pageCercaWP = new PageCercaWP(driver);
        pageCercaWP.cerca("management", hover);

        boolean textCercaPresent = pageCercaWP.comprovar("management", hover);

        assertTrue(textCercaPresent == true);

    }
}
