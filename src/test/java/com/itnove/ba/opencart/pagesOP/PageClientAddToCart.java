package com.itnove.ba.opencart.pagesOP;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class PageClientAddToCart {


    //@FindBy(xpath = "id(\"cart\")/ul/li[2]/div/p/a[2]")
    //public WebElement checkOutInDropdown;

    //@FindBy(xpath = "id(\"top-links\")/ul[1]/li[5]/a[1]")
    //public WebElement checkOutInTopLinks;


    public void addToCart(WebDriver driver, Actions hover) throws InterruptedException {

        List<WebElement> resultatsCerca = driver.findElements
                (By.xpath("id(\"content\")/div[3]/div/div/div[2]/div/h4/a"));

            for (int i = 1; i < (resultatsCerca.size()+1); i++) {

                WebElement addToCartButton = driver.findElement
                        (By.xpath("id(\"content\")/div[3]/div[" + i + "]/div/div[2]/div/button[1]"));
                //hover.moveToElement(addToCartButton).click().build().perform();
                addToCartButton.click();

                Thread.sleep(3000);

            }


        WebElement cartButtonDropdown = driver.findElement(By.xpath("id(\"cart\")/button[1]"));
        cartButtonDropdown.click();
        Thread.sleep(2000);
    }

    public String[] catchDataFromCheckOutDropdown(WebDriver driver) throws InterruptedException {

        List<WebElement> dropdownCartButton = driver.findElements
                (By.xpath("id('cart')/ul[1]/li[1]/table[1]/tbody[1]/tr"));

        int numberElementsInCart = dropdownCartButton.size();
        String numberElementsInCartString = "" + numberElementsInCart;

        WebElement totalPriceBox = driver.findElement
                (By.xpath("id(\"cart\")/ul[1]/li[2]/div[1]/table[1]/tbody[1]/tr[4]/td[2]"));

        String totalPrice = totalPriceBox.getText();

        String[] dataFromCheckOutDropdown = {numberElementsInCartString, totalPrice};

        return dataFromCheckOutDropdown;

    }

    public void clickCheckOutDropdown(WebDriver driver) throws InterruptedException {

        Actions build = new Actions(driver);

        WebElement checkOutInDropdown = driver.findElement(By.xpath("id(\"cart\")/ul/li[2]/div/p/a[2]"));
        build.moveToElement(checkOutInDropdown).build().perform();
        checkOutInDropdown.click();
        Thread.sleep(2000);
    }

    public void clickCheckOutTopLinks(WebDriver driver) throws InterruptedException {

        Actions build = new Actions(driver);

        WebElement checkOutInTopLinks = driver.findElement(By.xpath("id(\"cart\")/ul/li[2]/div/p/a[2]"));
        build.moveToElement(checkOutInTopLinks).build().perform();
        checkOutInTopLinks.click();
        Thread.sleep(2000);
    }
}
