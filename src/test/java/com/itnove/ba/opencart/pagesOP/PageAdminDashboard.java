package com.itnove.ba.opencart.pagesOP;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class PageAdminDashboard {

    @FindBy(id = "menu-catalog")
    public WebElement catalogPestanya;

    @FindBy(xpath = "id(\"collapse1\")/li[1]/a")
    public WebElement catalogDropdownCategories;



    //@FindBy(xpath = "id(\"top-links\")/ul[1]/li[5]/a[1]")
    //public WebElement checkOutInTopLinks;


    public void alertWindow(WebDriver driver) throws InterruptedException {

        WebElement exitAlertWindow = driver.findElement(By.xpath ("id('modal-security')/div/div/div/button"));

        Thread.sleep(1300);
        exitAlertWindow.click();

    }

    //NO M'HA DONAT MÉS TEMPS
    /*public void selectCatalogOptionFromDashboard(WebDriver driver, WebDriverWait wait, Actions hover) throws InterruptedException {

        WebElement catalogPestanya = driver.findElement(By.id("menu-catalog"));
        hover.moveToElement(catalogPestanya).click().build().perform();
        //wait.until(ExpectedConditions.visibilityOf(catalogDropdownCategories));
        Thread.sleep(1300);

        List<WebElement> catalogPestanyaDropdown = driver.findElements(By.xpath(catalogPestanya + "/ul"));


    }*/
}
