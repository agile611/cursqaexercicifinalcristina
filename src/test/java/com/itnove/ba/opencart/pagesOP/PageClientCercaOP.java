package com.itnove.ba.opencart.pagesOP;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageClientCercaOP {

    private WebDriver driver;
    public WebDriverWait wait;

    @FindBy(xpath = "id(\"search\")/input")
    public WebElement quadreCerca;

    @FindBy(xpath = "id(\"search\")/span/button")
    public WebElement iconaLupa;

    @FindBy(xpath = "id(\"input-search\")")
    public WebElement quadreCerca2;

    @FindBy(xpath = "id(\"content\")/div[3]/div[1]/div[1]/div[2]/div/h4/a")
    public WebElement resultat1;

    @FindBy(xpath = "id(\"content\")/div[3]")
    public WebElement allResults;


    public void cerca(String elementCerca, Actions hover) throws InterruptedException {

        quadreCerca.click();
        quadreCerca.sendKeys(elementCerca);

        hover.moveToElement(iconaLupa).click().build().perform();

        Thread.sleep(3000);

    }

    public PageClientCercaOP(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;}

}

