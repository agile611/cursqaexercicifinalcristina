package com.itnove.ba.opencart.pagesOP;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class PageClientCheckoutOP {

    private WebDriver driver;
    public WebDriverWait wait;

    //BILLING DETAILS

    @FindBy(id = "collapse-payment-address")
    public WebElement billingDetailsBody;

    @FindBy(id = "input-payment-firstname")
    public WebElement firstNameTextBox;

    @FindBy(id = "input-payment-lastname")
    public WebElement lastNameTextBox;

    @FindBy(id = "input-payment-company")
    public WebElement companyTextBox;

    @FindBy(id = "input-payment-address-1")
    public WebElement address1TextBox;

    @FindBy(id = "input-payment-address-2")
    public WebElement address2TextBox;

    @FindBy(id = "input-payment-city")
    public WebElement cityTextBox;

    @FindBy(id = "input-payment-postcode")
    public WebElement postcodeTextBox;

        //@FindBy(id = "input-payment-zone")
        //public WebElement regionStateDropdown;

    @FindBy(id = "button-payment-address")
    public WebElement continuePaymentButton;

    //DELIVERY DETAILS

    @FindBy(id = "collapse-shipping-address")
    public WebElement deliveryDetailsBody;

    @FindBy(xpath = "id(\"collapse-shipping-address\")/div/form/div[1]/label/input")
    public WebElement useExistingAddress;

    @FindBy(id = "button-shipping-address")
    public WebElement continueShippingButton;

    //DELIVERY METHOD

    @FindBy(id = "collapse-shipping-method")
    public WebElement deliveryMethodBody;

    @FindBy(xpath = "id(\"collapse-shipping-method\")/div/div/label/input")
    public WebElement flatShippingRate;

    @FindBy(xpath = "id(\"collapse-shipping-method\")/div/p[4]/textarea")
    public WebElement deliveryMethodComments;

    @FindBy(id = "button-shipping-method")
    public WebElement continueShippingMethodButton;

    //PAYMENT METHOD
    @FindBy(id = "collapse-payment-method")
    public WebElement paymentMethodBody;

    @FindBy(xpath = "id(\"collapse-payment-method\")/div/div/label/input")
    public WebElement paymentMethodCashOnDelivery;

    @FindBy(xpath = "id(\"collapse-payment-method\")/div/p[3]/textarea")
    public WebElement paymentMethodComments;

    @FindBy(xpath = "id(\"collapse-payment-method\")/div/div[2]/div/input[1]")
    public WebElement paymentMethodPrivacyPolicy;

    @FindBy(id = "button-payment-method")
    public WebElement continuePaymentMethodButton;

    //CONFIRM ORDER

    @FindBy(xpath = "id(\"collapse-checkout-confirm\")/div[1]/div[1]/table[1]/tfoot[1]/tr[5]/td[2]")
    public WebElement totalPriceBoxAtEnd;

    @FindBy(id = "collapse-checkout-confirm")
    public WebElement confirmOrderBody;

    @FindBy(id = "button-confirm")
    public WebElement confirmButton;

    public void checkoutCorrecteBillingDetails(Actions hover) throws InterruptedException {


        firstNameTextBox.click();
        firstNameTextBox.sendKeys("Sally");

        lastNameTextBox.click();
        lastNameTextBox.sendKeys("Mathews");

        companyTextBox.click();
        companyTextBox.sendKeys("Sit and Order");

        address1TextBox.click();
        address1TextBox.sendKeys("Main Street, 13");

        address2TextBox.click();
        address2TextBox.sendKeys("Other Street, 13, 08030, Barcelona");

        cityTextBox.click();
        cityTextBox.sendKeys("Barcelona");

        postcodeTextBox.click();
        postcodeTextBox.sendKeys("08030");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().equalsIgnoreCase("spain")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().equalsIgnoreCase("barcelona")) {

                regionOption.click();
                break outerloop2;

            }

        }

    hover.moveToElement(continuePaymentButton).click().build().perform();
    Thread.sleep(6000);

    }

    public void checkoutCorrecteBillingDetails_MandatoryFieldsOnly(Actions hover) throws InterruptedException {


        firstNameTextBox.click();
        firstNameTextBox.sendKeys("Sally");

        lastNameTextBox.click();
        lastNameTextBox.sendKeys("Mathews");

        address1TextBox.click();
        address1TextBox.sendKeys("Main Street, 13");

        cityTextBox.click();
        cityTextBox.sendKeys("Barcelona");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().equalsIgnoreCase("spain")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().equalsIgnoreCase("barcelona")) {

                regionOption.click();
                break outerloop2;

            }

        }

        hover.moveToElement(continuePaymentButton).click().build().perform();
        Thread.sleep(6000);

    }

    public void checkoutIncorrecteBillingDetails_FirstNameMissing(Actions hover) throws InterruptedException {

        Thread.sleep(4000);
        lastNameTextBox.click();
        lastNameTextBox.sendKeys("Mathews");

        companyTextBox.click();
        companyTextBox.sendKeys("Sit and Order");

        address1TextBox.click();
        address1TextBox.sendKeys("Main Street, 13");

        address2TextBox.click();
        address2TextBox.sendKeys("Other Street, 13, 08030, Barcelona");

        cityTextBox.click();
        cityTextBox.sendKeys("Barcelona");

        postcodeTextBox.click();
        postcodeTextBox.sendKeys("08030");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().equalsIgnoreCase("spain")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().equalsIgnoreCase("barcelona")) {

                regionOption.click();
                break outerloop2;

            }

        }

        hover.moveToElement(continuePaymentButton).click().build().perform();

        firstNameTextBox.click();

        Thread.sleep(6000);

    }

    public void checkoutIncorrecteBillingDetails_LastNameMissing(Actions hover) throws InterruptedException {


        firstNameTextBox.click();
        firstNameTextBox.sendKeys("Sally");

        companyTextBox.click();
        companyTextBox.sendKeys("Sit and Order");

        address1TextBox.click();
        address1TextBox.sendKeys("Main Street, 13");

        address2TextBox.click();
        address2TextBox.sendKeys("Other Street, 13, 08030, Barcelona");

        cityTextBox.click();
        cityTextBox.sendKeys("Barcelona");

        postcodeTextBox.click();
        postcodeTextBox.sendKeys("08030");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().equalsIgnoreCase("spain")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().equalsIgnoreCase("barcelona")) {

                regionOption.click();
                break outerloop2;

            }

        }

        hover.moveToElement(continuePaymentButton).click().build().perform();
        Thread.sleep(6000);

    }

    public void checkoutIncorrecteBillingDetails_Address1Missing(Actions hover) throws InterruptedException {


        firstNameTextBox.click();
        firstNameTextBox.sendKeys("Sally");

        lastNameTextBox.click();
        lastNameTextBox.sendKeys("Mathews");

        companyTextBox.click();
        companyTextBox.sendKeys("Sit and Order");

        address2TextBox.click();
        address2TextBox.sendKeys("Other Street, 13, 08030, Barcelona");

        cityTextBox.click();
        cityTextBox.sendKeys("Barcelona");

        postcodeTextBox.click();
        postcodeTextBox.sendKeys("08030");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().equalsIgnoreCase("spain")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().equalsIgnoreCase("barcelona")) {

                regionOption.click();
                break outerloop2;

            }

        }

        hover.moveToElement(continuePaymentButton).click().build().perform();
        Thread.sleep(6000);

    }

    public void checkoutIncorrecteBillingDetails_CityMissing(Actions hover) throws InterruptedException {


        firstNameTextBox.click();
        firstNameTextBox.sendKeys("Sally");

        lastNameTextBox.click();
        lastNameTextBox.sendKeys("Mathews");

        companyTextBox.click();
        companyTextBox.sendKeys("Sit and Order");

        address1TextBox.click();
        address1TextBox.sendKeys("Main Street, 13");

        address2TextBox.click();
        address2TextBox.sendKeys("Other Street, 13, 08030, Barcelona");

        postcodeTextBox.click();
        postcodeTextBox.sendKeys("08030");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().equalsIgnoreCase("spain")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().equalsIgnoreCase("barcelona")) {

                regionOption.click();
                break outerloop2;

            }

        }

        hover.moveToElement(continuePaymentButton).click().build().perform();
        Thread.sleep(6000);

    }

    public void checkoutIncorrecteBillingDetails_CountryMissing(Actions hover) throws InterruptedException {


        firstNameTextBox.click();
        firstNameTextBox.sendKeys("Sally");

        lastNameTextBox.click();
        lastNameTextBox.sendKeys("Mathews");

        companyTextBox.click();
        companyTextBox.sendKeys("Sit and Order");

        address1TextBox.click();
        address1TextBox.sendKeys("Main Street, 13");

        address2TextBox.click();
        address2TextBox.sendKeys("Other Street, 13, 08030, Barcelona");

        cityTextBox.click();
        cityTextBox.sendKeys("Barcelona");

        postcodeTextBox.click();
        postcodeTextBox.sendKeys("08030");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().toLowerCase().contains("please")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().equalsIgnoreCase("barcelona")) {

                regionOption.click();
                break outerloop2;

            }

        }

        hover.moveToElement(continuePaymentButton).click().build().perform();
        Thread.sleep(6000);

    }

    public void checkoutIncorrecteBillingDetails_RegionMissing(Actions hover) throws InterruptedException {


        firstNameTextBox.click();
        firstNameTextBox.sendKeys("Sally");

        lastNameTextBox.click();
        lastNameTextBox.sendKeys("Mathews");

        companyTextBox.click();
        companyTextBox.sendKeys("Sit and Order");

        address1TextBox.click();
        address1TextBox.sendKeys("Main Street, 13");

        address2TextBox.click();
        address2TextBox.sendKeys("Other Street, 13, 08030, Barcelona");

        cityTextBox.click();
        cityTextBox.sendKeys("Barcelona");

        postcodeTextBox.click();
        postcodeTextBox.sendKeys("08030");

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //countryDropdown.click();
        //countryDropdown.sendKeys("spain");
        //countryDropdown.submit();

        List<WebElement> countriesDropdown = driver.findElements(By.xpath("id(\"input-payment-country\")/option"));

        outerloop1:
        for (int i = 1; i < (countriesDropdown.size() + 1); i++) {

            WebElement countryOption = driver.findElement(By.xpath("id(\"input-payment-country\")/option[" + i + "]"));

            if (countryOption.getText().equalsIgnoreCase("spain")) {

                countryOption.click();
                break outerloop1;

            }
        }


        Thread.sleep(3000);

        //FOR SOME REASON THIS IMPLEMENTATION DOESN'T WORK, SO I'LL GO THE LONG WAY
        //regionStateDropdown.click();
        //regionStateDropdown.sendKeys("barcelona");
        //regionStateDropdown.submit();

        List<WebElement> regionsDropdown = driver.findElements(By.xpath("id(\"input-payment-zone\")/option"));

        outerloop2:
        for (int i = 1; i < (regionsDropdown.size() + 1); i++) {

            WebElement regionOption = driver.findElement(By.xpath("id(\"input-payment-zone\")/option[" + i + "]"));

            if (regionOption.getText().toLowerCase().contains("please")) {

                regionOption.click();
                break outerloop2;

            }

        }

        hover.moveToElement(continuePaymentButton).click().build().perform();
        Thread.sleep(6000);

    }

    public void checkOutCorrecteDeliveryDetails(Actions hover) throws InterruptedException {

        useExistingAddress.click();
        hover.moveToElement(continueShippingButton).click().build().perform();

        Thread.sleep(2000);

    }

    public void checkOutCorrecteDeliveryMethod() throws InterruptedException {

        flatShippingRate.click();
        deliveryMethodComments.click();
        deliveryMethodComments.sendKeys("Send it fast!");
        continueShippingMethodButton.click();
        Thread.sleep(2000);


    }

    public void checkOutCorrectePaymentMethod() throws InterruptedException {

        paymentMethodCashOnDelivery.click();
        paymentMethodComments.click();
        paymentMethodComments.sendKeys("I'll pay in 5 cents. coins");
        paymentMethodPrivacyPolicy.click();
        continuePaymentMethodButton.click();
        Thread.sleep(2000);

    }

    public void checkOutIncorrectePaymentMethod_PrivacyPolicyMissing() throws InterruptedException {

        paymentMethodCashOnDelivery.click();
        paymentMethodComments.click();
        paymentMethodComments.sendKeys("I'll pay in 5 cents. coins");

        continuePaymentMethodButton.click();
        Thread.sleep(2000);

    }

    public String[] checkOutCorrecteConfirmOrder(Actions hover) throws InterruptedException {

        List<WebElement> elementsInCartAtEnd = driver.findElements
                (By.xpath("id(\"collapse-checkout-confirm\")/div/div/table/tbody/tr"));
        int numberElementsInCartAtEnd = elementsInCartAtEnd.size();
        String numberElementsInCartAtEndString = "" + numberElementsInCartAtEnd;

        String totalPriceAtEnd = totalPriceBoxAtEnd.getText();

        String[] dataConfirmOrder = {numberElementsInCartAtEndString, totalPriceAtEnd};

        hover.moveToElement(confirmButton).click().build().perform();

        Thread.sleep(2000);

        return dataConfirmOrder;

    }

    public PageClientCheckoutOP(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;}

}

