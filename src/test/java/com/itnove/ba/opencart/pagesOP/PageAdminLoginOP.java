package com.itnove.ba.opencart.pagesOP;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageAdminLoginOP {

    private WebDriver driver;
    public WebDriverWait wait;

    @FindBy(id = "input-username")
    public WebElement quadreTextUser;

    @FindBy(id = "input-password")
    public WebElement quadreTextPassword;

    @FindBy(xpath = "id(\"content\")/div/div/div/div/div[2]/form/div[3]/button")
    public WebElement loginButton;


    public void login(String user, String pswd, Actions hover) throws InterruptedException {

        driver.navigate().to("http://opencart.votarem.lu/admin");

        quadreTextUser.click();
        quadreTextUser.sendKeys(user);
        quadreTextPassword.click();
        quadreTextPassword.sendKeys(pswd);
        loginButton.click();

        Thread.sleep(4000);

    }

    public PageAdminLoginOP(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;}

}

