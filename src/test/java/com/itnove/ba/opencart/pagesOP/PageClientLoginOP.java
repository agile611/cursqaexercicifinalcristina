package com.itnove.ba.opencart.pagesOP;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PageClientLoginOP {

    private WebDriver driver;
    public WebDriverWait wait;

    @FindBy(xpath = "id(\"top-links\")/ul[1]/li[2]/a[1]/span[1]")
    public WebElement myAccountButton;

    @FindBy(xpath = "id(\"top-links\")/ul/li[2]/ul/li[2]/a")
    public WebElement loginOption;

    @FindBy(id = "input-email")
    public WebElement quadreTextEmail;

    @FindBy(id = "input-password")
    public WebElement quadreTextPassword;

    @FindBy(xpath = "id(\"content\")/div[1]/div[2]/div[1]/form[1]/input[1]")
    public WebElement loginButton;


    public void login(String email, String pswd, Actions hover) throws InterruptedException {

        driver.navigate().to("http://opencart.votarem.lu");

        hover.moveToElement(myAccountButton).click().build().perform();
        //wait.until(ExpectedConditions.visibilityOf(myAccountDesplegable));

        Thread.sleep(2000);
        hover.moveToElement(loginOption).click().build().perform();

        Thread.sleep(2000);

        //wait.until(ExpectedConditions.urlContains("login"));

        quadreTextEmail.click();
        quadreTextEmail.sendKeys(email);
        quadreTextPassword.click();
        quadreTextPassword.sendKeys(pswd);

        Thread.sleep(2000);


        loginButton.click();

        Thread.sleep(4000);

    }

    public PageClientLoginOP(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;}

}

