package com.itnove.ba.opencart.computerOP.AdminLoginOP;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageAdminLoginOP;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class OP22AdminLogin_incorrecte_ValidUserWrongPassword extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageAdminLoginOP pageAdminLoginOP = new PageAdminLoginOP(driver);
        pageAdminLoginOP.login("user", "bitnami11", hover);
        assertFalse((driver.getCurrentUrl().contains("dashboard")));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/OP/OP22_FF.png"));
    }
}
