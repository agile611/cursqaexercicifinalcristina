package com.itnove.ba.opencart.computerOP.client_cerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientCercaOP;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class OP5ClientCerca_Correcte extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        //LOGIN
        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("sallymathews@barcelonaactiva.cat", "bitnami", hover);
        assertTrue((driver.getCurrentUrl().contains("account")));

        //CERCAR
        PageClientCercaOP pageClientCercaOP = new PageClientCercaOP(driver);
        pageClientCercaOP.cerca("samsung", hover);

        assertTrue(pageClientCercaOP.quadreCerca2.getAttribute("value").contains("samsung"));
        assertTrue(pageClientCercaOP.resultat1.getText().toLowerCase().contains("samsung"));
    }

}
