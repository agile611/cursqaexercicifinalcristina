package com.itnove.ba.opencart.computerOP.client_cerca;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientCercaOP;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertTrue;

public class OP8ClientCerca_Incorrecte_Números extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("sallymathews@barcelonaactiva.cat", "bitnami", hover);
        assertTrue((driver.getCurrentUrl().contains("account")));

        PageClientCercaOP pageClientCercaOP = new PageClientCercaOP(driver);
        pageClientCercaOP.cerca("9999999999999", hover);

        assertTrue(pageClientCercaOP.quadreCerca2.getAttribute("value").contains("9999999999999"));
        //assertFalse(pageClientCercaOP.allResults.isDisplayed());

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/OP/OP8_FF.png"));

    }

}
