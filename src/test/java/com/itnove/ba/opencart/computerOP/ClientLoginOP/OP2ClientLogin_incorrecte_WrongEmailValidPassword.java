package com.itnove.ba.opencart.computerOP.ClientLoginOP;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class OP2ClientLogin_incorrecte_WrongEmailValidPassword extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("1111@barcelonaactiva.cat", "bitnami", hover);

        assertFalse((driver.getCurrentUrl().contains("logout")));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/OP/OP2_FF.png"));

    }
}
