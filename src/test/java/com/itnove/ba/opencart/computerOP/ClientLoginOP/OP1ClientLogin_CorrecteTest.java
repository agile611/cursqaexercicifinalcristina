package com.itnove.ba.opencart.computerOP.ClientLoginOP;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class OP1ClientLogin_CorrecteTest extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("sallymathews@barcelonaactiva.cat", "bitnami", hover);
        assertTrue((driver.getCurrentUrl().contains("account")));
    }
}
