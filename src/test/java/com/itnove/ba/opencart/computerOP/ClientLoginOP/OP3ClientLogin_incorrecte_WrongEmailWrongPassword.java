package com.itnove.ba.opencart.computerOP.ClientLoginOP;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertFalse;

public class OP3ClientLogin_incorrecte_WrongEmailWrongPassword extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("1111@barcelonaactiva.cat", "1111", hover);

        assertFalse((driver.getCurrentUrl().contains("logout")));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/OP/OP3_FF.png"));
    }
}
