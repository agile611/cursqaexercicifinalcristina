package com.itnove.ba.opencart.computerOP.client_checkout;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientAddToCart;
import com.itnove.ba.opencart.pagesOP.PageClientCercaOP;
import com.itnove.ba.opencart.pagesOP.PageClientCheckoutOP;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class OP18ClientCheckout_Incorrecte_PrivacyPolicy extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        //LOGIN
        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("lolamathews@barcelonaactiva.cat", "bitnami", hover);
        Assert.assertTrue(driver.getCurrentUrl().contains("account"));

        //CERCA
        PageClientCercaOP pageClientCercaOP = new PageClientCercaOP(driver);
        pageClientCercaOP.cerca("sony", hover);
        Assert.assertTrue(driver.getCurrentUrl().contains("search"));

        //AFEGIR AL CARRET i CLICAR CHECKOUT
        PageClientAddToCart pageClientAddToCart = new PageClientAddToCart();
        pageClientAddToCart.addToCart(driver, hover);
        pageClientAddToCart.clickCheckOutTopLinks(driver);
        Assert.assertTrue(driver.getCurrentUrl().contains("checkout"));

        //BILLING DETAILS: OMPLIR
        PageClientCheckoutOP pageClientCheckoutOP = new PageClientCheckoutOP(driver);

        String billingDetailsBodyString = pageClientCheckoutOP.billingDetailsBody.getAttribute("aria-expanded");
        Assert.assertTrue((billingDetailsBodyString !=null) &&
                (billingDetailsBodyString.equalsIgnoreCase("true")));

        pageClientCheckoutOP.checkoutCorrecteBillingDetails(hover);

        //DELIVERY DETAILS: OMPLIR

        String deliveryDetailsBodyString = pageClientCheckoutOP.deliveryDetailsBody.getAttribute("aria-expanded");
        Assert.assertTrue((deliveryDetailsBodyString !=null) &&
                (deliveryDetailsBodyString.equalsIgnoreCase("true")));

        pageClientCheckoutOP.checkOutCorrecteDeliveryDetails(hover);

        //DELIVERY METHOD: OMPLIR

        String deliveryMethodBodyString = pageClientCheckoutOP.deliveryMethodBody.getAttribute("aria-expanded");
        Assert.assertTrue((deliveryMethodBodyString !=null) &&
                (deliveryMethodBodyString.equalsIgnoreCase("true")));

        pageClientCheckoutOP.checkOutCorrecteDeliveryMethod();

        //PAYMENT METHOD: OMPLIR
        String paymentMethodBodyString = pageClientCheckoutOP.paymentMethodBody.getAttribute("aria-expanded");
        Assert.assertTrue((paymentMethodBodyString !=null) &&
                (paymentMethodBodyString.equalsIgnoreCase("true")));

        pageClientCheckoutOP.checkOutCorrectePaymentMethod();

        //CONFIRM ORDER: CONFIRM
        String confirmOrderBodyString = pageClientCheckoutOP.confirmOrderBody.getAttribute("aria-expanded");
        Assert.assertTrue((confirmOrderBodyString !=null) &&
                (confirmOrderBodyString.equalsIgnoreCase("true")));

        pageClientCheckoutOP.checkOutIncorrectePaymentMethod_PrivacyPolicyMissing();

        Assert.assertFalse(driver.getCurrentUrl().contains("success"));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/OP/OP18_FF.png"));

    }
}