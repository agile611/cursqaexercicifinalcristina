package com.itnove.ba.opencart.computerOP.client_checkout;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientAddToCart;
import com.itnove.ba.opencart.pagesOP.PageClientCercaOP;
import com.itnove.ba.opencart.pagesOP.PageClientCheckoutOP;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

public class OP16ClientCheckout_Incorrecte_MandatoryFields_Country extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException, IOException {

        //LOGIN
        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("johnmathews@barcelonaactiva.cat", "bitnami", hover);
        Assert.assertTrue(driver.getCurrentUrl().contains("account"));

        //CERCA
        PageClientCercaOP pageClientCercaOP = new PageClientCercaOP(driver);
        pageClientCercaOP.cerca("sony", hover);
        Assert.assertTrue(driver.getCurrentUrl().contains("search"));

        //AFEGIR AL CARRET i CLICAR CHECKOUT
        PageClientAddToCart pageClientAddToCart = new PageClientAddToCart();
        pageClientAddToCart.addToCart(driver, hover);
        pageClientAddToCart.clickCheckOutTopLinks(driver);
        Assert.assertTrue(driver.getCurrentUrl().contains("checkout"));

        //BILLING DETAILS: OMPLIR
        PageClientCheckoutOP pageClientCheckoutOP = new PageClientCheckoutOP(driver);

        String billingDetailsBodyString = pageClientCheckoutOP.billingDetailsBody.getAttribute("aria-expanded");
        Assert.assertTrue((billingDetailsBodyString !=null) &&
                (billingDetailsBodyString.equalsIgnoreCase("true")));

        pageClientCheckoutOP.checkoutIncorrecteBillingDetails_CountryMissing(hover);

        //DELIVERY DETAILS: COMPROVAR QUE NO HI ARRIBEM

        String deliveryDetailsBodyString = pageClientCheckoutOP.deliveryDetailsBody.getAttribute("aria-expanded");
        Assert.assertFalse((deliveryDetailsBodyString !=null) &&
                (deliveryDetailsBodyString.equalsIgnoreCase("true")));

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("/Users/nasrodo/capturesTest/OP/OP16_FF.png"));

    }
}