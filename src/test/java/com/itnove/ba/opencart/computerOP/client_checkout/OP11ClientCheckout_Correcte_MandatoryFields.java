package com.itnove.ba.opencart.computerOP.client_checkout;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageClientAddToCart;
import com.itnove.ba.opencart.pagesOP.PageClientCercaOP;
import com.itnove.ba.opencart.pagesOP.PageClientCheckoutOP;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.testng.Assert;
import org.testng.annotations.Test;

public class OP11ClientCheckout_Correcte_MandatoryFields extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        //LOGIN
        PageClientLoginOP pageClientLoginOP = new PageClientLoginOP(driver);
        pageClientLoginOP.login("sallymathews@barcelonaactiva.cat", "bitnami", hover);
        Assert.assertTrue(driver.getCurrentUrl().contains("account"));

        //CERCA
        PageClientCercaOP pageClientCercaOP = new PageClientCercaOP(driver);
        pageClientCercaOP.cerca("sony", hover);
        Assert.assertTrue(driver.getCurrentUrl().contains("search"));

        //AFEGIR AL CARRET i CLICAR CHECKOUT
        PageClientAddToCart pageClientAddToCart = new PageClientAddToCart();
        pageClientAddToCart.addToCart(driver, hover);
        pageClientAddToCart.clickCheckOutTopLinks(driver);
        Assert.assertTrue(driver.getCurrentUrl().contains("checkout"));

        //BILLING DETAILS: OMPLIR
        PageClientCheckoutOP pageClientCheckoutOP = new PageClientCheckoutOP(driver);

        String billingDetailsBodyString = pageClientCheckoutOP.billingDetailsBody.getAttribute("aria-expanded");
        Assert.assertTrue((billingDetailsBodyString !=null) &&
                (billingDetailsBodyString.equalsIgnoreCase("true")));

        pageClientCheckoutOP.checkoutCorrecteBillingDetails_MandatoryFieldsOnly(hover);

        //DELIVERY DETAILS: COMPROVAR QUE HI ARRIBEM

        String deliveryDetailsBodyString = pageClientCheckoutOP.deliveryDetailsBody.getAttribute("aria-expanded");
        Assert.assertTrue((deliveryDetailsBodyString !=null) &&
                (deliveryDetailsBodyString.equalsIgnoreCase("true")));

    }
}
