package com.itnove.ba.opencart.computerOP.AdminLoginOP;

import com.itnove.ba.BaseTest;
import com.itnove.ba.opencart.pagesOP.PageAdminLoginOP;
import com.itnove.ba.opencart.pagesOP.PageClientLoginOP;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class OP19AdminLogin_CorrecteTest extends BaseTest {

    @Test
    public void testAppComp() throws InterruptedException {

        PageAdminLoginOP pageAdminLoginOP = new PageAdminLoginOP(driver);
        pageAdminLoginOP.login("user", "bitnami1", hover);
        assertTrue((driver.getCurrentUrl().contains("dashboard")));
    }
}
