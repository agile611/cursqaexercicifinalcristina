package com.itnove.ba;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by guillemhs on 2017-02-16.
 */
public class BaseTestSauce {
    public RemoteWebDriver driver;
    public WebDriverWait wait;

    @Before
    public void setUp() throws MalformedURLException {
        String device = System.getProperty("device");
        // switch between diffrent browsers, e.g. iOS Safari or Android Chrome
        // let's use the os name to differentiate, because we only use default browser in that os
        if (device != null && device.equalsIgnoreCase("android")) {
            DesiredCapabilities caps = DesiredCapabilities.android();
            caps.setCapability("appiumVersion", "1.6.4");
            caps.setCapability("deviceName","Android Emulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("browserName", "Browser");
            caps.setCapability("platformVersion", "5.1");
            caps.setCapability("platformName","Android");
            driver = new AndroidDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        } else {
            DesiredCapabilities caps = DesiredCapabilities.iphone();
            caps.setCapability("appiumVersion", "1.7.1");
            caps.setCapability("deviceName", "iPhone 6 Plus Simulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("platformVersion", "10.0");
            caps.setCapability("platformName", "iOS");
            caps.setCapability("browserName", "Safari");
            driver = new IOSDriver(new URL("http://itnove:4394d787-3244-4c03-9490-3816f2bb683b@ondemand.saucelabs.com:80/wd/hub"), caps);
        }
        wait = new WebDriverWait(driver, 10);

    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}
